package com.example.passandodadosactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class DadosInfo extends AppCompatActivity {
    TextView textNome;
    TextView textIdade;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dados_info);

        textNome = findViewById(R.id.Nome);
        textIdade = findViewById(R.id.idade);

        Bundle dados = getIntent().getExtras();
        String nome = dados.getString("Nome");
        int idade = dados.getInt("Idade");

        textNome.setText(nome);
        textIdade.setText(String.valueOf(idade));


    }
}